export const SHORT_DATE_FORMAT_WITHOUT_YEAR = {
  en: 'MMM D',
  de: 'D. MMM',
  fr: 'D MMM',
  es: 'D [de] MMM',
  it: 'D MMM',
  pt: 'D [de] MMM',
  ja: 'M月D日',
  zh: 'MMMD日',
};
