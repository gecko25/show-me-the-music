import App, { Container } from 'next/app';
import React from 'react';
import { Provider } from 'react-redux';
import Head from 'next/head';

/* Redux */
import withReduxStore from '../store';

/* Translations */
import { appWithTranslation } from '../i18n';

/* Global styles */
import '../styles/variables.scss';
import '../styles/fonts.scss';
import '../styles/globals.scss';

/* Global components */
import { Header } from '../components';


/* eslint-disable max-len */
class MyApp extends App {
  render() {
    const { Component, pageProps, reduxStore } = this.props;
    return (
      <Container>
        <Provider store={reduxStore}>
          <div>
            <Head>
              <title>Show Me The Music | Explore Live Music Events | Preview concerts & create playlists</title>
              <meta name="description" content="Create a playlist of the bands and artists coming to your city. Listen those bands & artists to decide to see them live." />
              <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
              <link rel="canonical" href="https://www.showmethemusic.co" />
              <meta name="robots" content="index, follow" />
              <meta name="keywords" content="Live music, concerts, playlists, create playlist, city, listen, spotify, bandsintown, bands, artists, genres" />

              <meta property="og:type" content="article" />
              <meta property="og:title" content="Curate a playlist of the bands and artists coming to your city." />
              <meta property="og:description" content="Explore live music events by genre, neighborhood, artist popularity, venue size. Listen those bands & artists to decide whether or not see them live." />
              <meta property="og:image" content="https://www.showmethemusic.co/static/images/tickets.png" />
              <meta property="og:url" content="/" />
              <meta property="og:site_name" content="Show Me the Music" />

              <link rel="apple-touch-icon" sizes="57x57" href="/static/favicons/apple-icon-57x57.png" />
              <link rel="apple-touch-icon" sizes="60x60" href="/static/favicons/apple-icon-60x60.png" />
              <link rel="apple-touch-icon" sizes="72x72" href="/static/favicons/apple-icon-72x72.png" />
              <link rel="apple-touch-icon" sizes="76x76" href="/static/favicons/apple-icon-76x76.png" />
              <link rel="apple-touch-icon" sizes="114x114" href="/static/favicons/apple-icon-114x114.png" />
              <link rel="apple-touch-icon" sizes="120x120" href="/static/favicons/apple-icon-120x120.png" />
              <link rel="apple-touch-icon" sizes="144x144" href="/static/favicons/apple-icon-144x144.png" />
              <link rel="apple-touch-icon" sizes="152x152" href="/static/favicons/apple-icon-152x152.png" />
              <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-icon-180x180.png" />
              <link rel="icon" type="image/png" sizes="192x192" href="/static/favicons/android-icon-192x192.png" />
              <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
              <link rel="icon" type="image/png" sizes="96x96" href="/static/favicons/favicon-96x96.png" />
              <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
              <link rel="manifest" href="/manifest.json" />
              <meta name="msapplication-TileColor" content="#ffffff" />
              <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
              <meta name="theme-color" content="#ffffff" />
            </Head>

            <Header />
            <Component {...pageProps} />
          </div>
        </Provider>
      </Container>
    );
  }
}

export default appWithTranslation(withReduxStore(MyApp));
