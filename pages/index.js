/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */

import { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AwesomeDebouncePromise from 'awesome-debounce-promise';
import useConstant from 'use-constant';
import { Trans } from 'react-i18next';


import axios from 'axios';

import 'react-dates/initialize';
import '../styles/_datepicker.scss';
import './index.scss';

import { SingleDatePicker } from 'react-dates';
import moment from 'moment';
import { logger, absoluteUrl } from '../utils';

import {
  Error,
  Loader,
  EventCard,
} from '../components';

import { setEvents, setBaseUrl } from '../store/actions';
import { getEventsFromIp, getLocations, getEventsFromLocation } from '../store/api';

const Index = ({
  loading,
  error,
  errorMessage,
  events,
  baseUrl,
  setEvents,
}) => {
  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error message={errorMessage} />;
  }

  /* *******
   * HOOKS *
   ******* */
  const [date, setDate] = useState(moment());
  const [focused, setFocus] = useState(false);
  const [location, updateLocation] = useState(events[0].location.city);
  const [searchingLocations, isSearchingLocations] = useState(false);
  const [lastLocation, updateLastLocation] = useState(events[0].location.city);
  const [locations, setLocations] = useState([]);
  const [searchingEvents, isSearchingEvents] = useState(false);


  /* ********************
   * SEARCHING LOCATION *
   ******************** */
  const searchAPI = query => (getLocations({ query, baseUrl }));

  // Create a constant debounced function (created only once per component instance)
  const searchAPIDebounced = useConstant(() => AwesomeDebouncePromise(searchAPI, 300));

  const handleTextChange = async (e) => {
    isSearchingLocations(true);
    updateLocation(e.target.value);

    if (e.target.value === '') {
      setLocations([]);
      isSearchingLocations(false);
    } else {
      const results = await searchAPIDebounced(e.target.value);
      setLocations(results);
      isSearchingLocations(false);
    }
  };

  const onLocationBlur = () => {
    if (location === '') {
      updateLocation(lastLocation);
    }
  };

  const clearInput = (e) => {
    e.preventDefault();
    updateLocation('');
    updateLastLocation(e.target.value);
  };

  /* ************************************
   * SEARCHING EVENTS FOR DATE/LOCATION *
   ************************************ */
  const onDateChange = ({ date }) => {
    setDate(date);
    // TODO: search events again
  };

  const searchEvents = async (e, loc) => {
    isSearchingLocations(true);
    if (e.type === 'keydown' || e.type === 'click') {
      logger.info('searching for ', loc);
      updateLocation(loc.displayName);
      setLocations([]);

      const eventsFromLocation = await getEventsFromLocation({ baseUrl, date, location: loc.id });
      setEvents(eventsFromLocation);
      isSearchingLocations(false);
    }
  };

  return (
    <div className="App">
      <section className="Search">
        <span className="marquee-text-light"><Trans>Show me the music</Trans></span>

        <SingleDatePicker
          date={date} // momentPropTypes.momentObj or null,
          onDateChange={onDateChange} // PropTypes.func.isRequired,
          focused={focused} // PropTypes.bool
          onFocusChange={({ focused }) => { setFocus(focused); }} // PropTypes.func.isRequired,
          id="selectedDate" // PropTypes.string.isRequired,
          displayFormat="MMM D" // D [de] MMM for espanol
          numberOfMonths={1}
          noBorder
        />

        <input
          className="Search__location-input marquee-text-bold"
          type="text"
          value={location}
          onFocus={clearInput}
          onBlur={onLocationBlur}
          onChange={handleTextChange}
          placeholder="Enter a city here"
        />

        <div>
          {
            searchingLocations
              ? <Loader />
              : locations.map(loc => (
                <div
                  className="Search__locationDropdown marquee-text-light"
                  key={loc.id}
                  role="button"
                  tabIndex="0"
                  onKeyDown={e => searchEvents.bind(null, e, loc)()}
                  onClick={e => searchEvents.bind(null, e, loc)()}
                >
                  {loc.displayName}
                </div>
              ))
          }
        </div>
      </section>

      <div className="results">
        {
          searchingEvents
            ? <Loader />
            : events.map(event => <EventCard key={event.id} event={event} />)
        }
      </div>
    </div>
  );
};


Index.getInitialProps = async function getInitialProps({ req, reduxStore }) {
  const initialPageLoad = reduxStore.getState().events.length <= 0;
  if (initialPageLoad) {
    const clientip = (req && req.headers && req.headers['x-real-ip']) || 0;

    const baseUrl = absoluteUrl(req, 'localhost:3000');
    logger.info('Setting baseUrl', baseUrl);
    reduxStore.dispatch(setBaseUrl(baseUrl));
    try {
      const events = await getEventsFromIp({ baseUrl, clientip });
      reduxStore.dispatch(setEvents(events));
    } catch (error) {
      logger.error('Unable to load events', error);
      return {
        events: [],
        error: true,
        errorMessage: error.message,
        loading: false,
      };
    }
  }
  logger.info('Loading events from redux', reduxStore.getState().events.length);
  return {
    events: reduxStore.getState().events,
    loading: false,
    error: false,
    errorMessage: '',
  };
};

const mapStateToProps = state => ({
  events: state.events,
  baseUrl: state.baseUrl,
});

Index.propTypes = {
  events: PropTypes.arrayOf(PropTypes.object),
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  loading: PropTypes.bool,
  baseUrl: PropTypes.string,
  setEvents: PropTypes.func,
};

Index.defaultProps = {
  events: [{}],
  error: false,
  errorMessage: '',
  loading: true,
  baseUrl: '',
  setEvents: PropTypes.func,
};

// https://medium.com/disdj/next-js-redux-in-an-easy-way-fae083aa037c ??? different way to use redux?
export default connect(mapStateToProps, { setEvents })(Index);
