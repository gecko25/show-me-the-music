import PropTypes from 'prop-types';
import axios from 'axios';
import moment from 'moment';
import Link from 'next/link';
import queryString from 'query-string';
import { Trans } from 'react-i18next';
import { Error, Loader, VenueMap } from '../components';
import { SHORT_DATE_FORMAT_WITHOUT_YEAR } from '../constants';
import { absoluteUrl, logger } from '../utils';
import './event.scss';

const Event = ({
  loading,
  error,
  errorMessage,
  event,
  genres,
  topTracks,
  followers,
  popularity,
  playUrl,
  similarArtists,
}) => {
  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error message={errorMessage} />;
  }

  return (
    <div>

      <div style={{ textAlign: 'center' }}>
        <h1>
          {event.displayName}
        </h1>
        { followers
          && (
            <div>
              <Trans>Followers</Trans>
              <span>:&nbsp;</span>
              {followers}
            </div>
          )
        }

        { popularity
          && (
            <div>
              <Trans>Popularity</Trans>
              <span>:&nbsp;</span>
              {popularity}
              %
            </div>
          )
        }


        { similarArtists.length > 0 &&
          (
            <div style={{ marginTop: '15px' }}>
              <Trans>Similar artists</Trans>
            </div>
          )
        }
        { similarArtists.map(artist => <div>{artist}</div>) }
        <h3>
          {moment(event.start.date).format(SHORT_DATE_FORMAT_WITHOUT_YEAR.en)}
          @
          {event.start.time}
        </h3>
      </div>

      <section className="genres" style={{ textAlign: 'center', maxWidth: '600px', margin: '20px auto' }}>
        {
          genres.map(genre => (
            <div
              style={{ display: 'inline-block' }}
              key={genre}
            >
              {' '}
              *
              {genre}
              {' '}
            </div>
          ))
        }
      </section>

      <div className="container">
        <section className="top-tracks" style={{ marginRight: '15px' }}>
          {
            topTracks.map(track => (
              <div key={track.id}>
                <iframe
                  key={track.id}
                  src={`https://open.spotify.com/embed/track/${track.id}`}
                  width="300"
                  height="80"
                  frameBorder="1"
                  allow="encrypted-media"
                  title={track.name}
                />
              </div>
            ))
          }

          <a href={playUrl} target="blank"><Trans>Listen to this artist in spotify</Trans></a>
        </section>

        <section className="venue-details" style={{ marginLeft: '15px' }}>
          <a href={event.venue.website}>
            {event.venue.displayName}
          </a>

          <section>
            { event.venue.street && <div>{event.venue.street}</div> }

            { event.venue.phone
              && (
                <div>
                  Call:
                  {event.venue.phone}
                </div>
              )
            }

            { event.venue.capacity
              && (
                <div>
                  Capacity:
                  &nbsp;
                  {event.venue.capacity}
                  &nbsp;people
                </div>
              )
            }

          </section>

          <VenueMap lat={event.venue.lat} lng={event.venue.lng} location={event.location} />
        </section>
      </div>

      <Link href="/">
        <button style={{ display: 'block' }} type="button"><Trans>Back</Trans></button>
      </Link>
    </div>
  );
};

Event.getInitialProps = async function getInitialProps({ query, req }) {
  const { eventId } = query;

  const baseUrl = absoluteUrl(req, 'localhost:3000');
  logger.info('Going to get event info: ', `${baseUrl}api/event?eventId=${eventId}`);
  const response = await axios(`${baseUrl}api/event?eventId=${eventId}`);
  const songkick = response.data;

  try {
    if (songkick.resultsPage.status !== 'ok') throw new Error('Songkick API problem.');
    logger.info('Successfully got event info', songkick.resultsPage.results.event);

    const { artist } = songkick.resultsPage.results.event.performance[0];

    const params = {
      songkickArtistId: artist.id,
      songkickArtistName: artist.displayName,
      mbid: artist.identifier.length >= 1 ? artist.identifier[0].mbid : null,
    };

    logger.info('Going to get artist info: ', `${baseUrl}api/artist?${queryString.stringify(params)}`);
    const response2 = await axios(`${baseUrl}api/artist?${queryString.stringify(params)}`);
    const artistInfo = response2.data;
    logger.info('Successfully received artist info on front end', artistInfo);

    return {
      error: false,
      loading: false,
      genres: artistInfo.genres,
      topTracks: artistInfo.spotify.top_tracks.splice(0, 3),
      followers: artistInfo.spotify.followers,
      popularity: artistInfo.spotify.popularity,
      playUrl: artistInfo.spotify.play_url,
      event: songkick.resultsPage.results.event,
      similarArtists: artistInfo.similarArtists,
    };
  } catch (error) {
    logger.error('Error getting the event info', error);

    return {
      event: {},
      error: true,
      loading: false,
      errorMessage: error.message,
    };
  }
};

Event.propTypes = {
  event: PropTypes.shape({
    id: 0,
    displayName: PropTypes.string,
    type: PropTypes.string,
    uri: PropTypes.string,
    status: PropTypes.string,
    popularity: PropTypes.number,
    start: PropTypes.object,
    performance: PropTypes.array,
    ageRestriction: PropTypes.string,
    flaggedAsEnded: PropTypes.bool,
    venue: PropTypes.object,
    location: PropTypes.object,
  }),
  genres: PropTypes.arrayOf(PropTypes.string),
  similarArtists: PropTypes.arrayOf(PropTypes.string),
  topTracks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  })),
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  loading: PropTypes.bool,
  followers: PropTypes.number,
  popularity: PropTypes.number,
  playUrl: PropTypes.string,
};

Event.defaultProps = {
  event: {
    id: 0,
    displayName: '',
    type: '',
    uri: '',
    status: '',
    popularity: 0.0001,
    start: {},
    performance: [],
    ageRestriction: '',
    flaggedAsEnded: false,
    venue: {},
    location: {},
  },
  genres: ['No genre information available'],
  similarArtists: [],
  topTracks: [{
    id: '',
    name: '',
  }],
  followers: 0,
  popularity: 0,
  playUrl: '',
  error: false,
  errorMessage: '',
  loading: true,
};

export default Event;
