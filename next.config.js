const withSass = require('@zeit/next-sass')

module.exports = withSass({
	target: 'serverless',
  sassLoaderOptions: {
    data: '@import "styles/variables.scss";',
  },
	webpack: (config, options) => {
		config.module.rules.push({
			enforce: "pre",
			test: /\.js$/,
			exclude: /node_modules/,
			include: /pages/,
			loader: "eslint-loader",
			options: {}
		});

    config.devtool = "inline-source-map",

		config.resolve.alias = {
			...config.resolve.alias,
			"%PAGES%": './pages',
			"%COMPONENTS%": './components',
		}

    config.node = {
      fs: "empty",
      child_process: "empty"
    };

		return config;
	},
  distDir: './.next'
});
