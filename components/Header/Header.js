import React from 'react';
import classnames from 'classnames';
import { i18n } from '../../i18n';
import { withTranslation } from '../../i18n';
import './Header.scss';

const Header = ({ t }) => {
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  }

  const styles = lng => {
    const classes = {
      btn: classnames('Header__language context-text', {
        'Header__language--selected': i18n.language === lng,
      }),
    }
    return classes;
  };

  return (
    <div className="Header">
      <button
        className={styles('en').btn}
        onClick={() => changeLanguage('en')}
      >
        English
      </button>

      <button
        className={styles('es').btn}
        onClick={() => changeLanguage('es')}
      >
        Español
      </button>
    </div>
  )
}

export default withTranslation()(Header);
