import PropTypes from 'prop-types';
import { Trans } from 'react-i18next';
import Link from 'next/link';
import Spinner from '../SVG/Spinner';
import './EventCard.scss';

const containerStyles = artistId => ({
  backgroundImage: `url(https://images.sk-static.com/images/media/profile_images/artists/${artistId}/huge_avatar)`
})

class EventCard extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      openingEventDetails: false,
    }
  }
  render() {
    const { openingEventDetails } = this.state;
    const { event } = this.props;

    return (
      <div className="event-info-card-container" onClick={() => { this.setState({ openingEventDetails: true })}}>
        {
          openingEventDetails ?
          <Spinner />
          :
          <Link href={`/event?eventId=${event.id}`}>
            <div className="event-info-card">
               <section className="event-details">
                 <div className="event-details-container">
                     <div className="artist-name detail artist-headline-text">{event.performers[0].displayName}</div>
                     <div className="venue detail artist-subtext">{event.venue.displayName}</div>
                     <div className="showdate detail artist-subtext">{event.start.displayDate} @</div>
                     <div className="showtime detail artist-subtext">{event.start.displayTime}</div>
                 </div>

                 <div
                    style={containerStyles(event.performers[0].songkick.artist_id)}
                    className="background-image-container"
                 />
              </section>

              <div className="see-more-details-text supporting-text"><Trans>Details</Trans></div>
            </div>
          </Link>
        }
      </div>
    )
  }

};

EventCard.propTypes = {
  event: PropTypes.shape({
    performers: PropTypes.array,
    start: PropTypes.object,
    venue: PropTypes.object,
    id: PropTypes.number,
  }),
};

EventCard.defaultProps = {
  event: {
    performers: [],
    start: [],
    venue: [],
    id: 0,
  },
};

export default EventCard;
