import PropTypes from 'prop-types';

const Error = ({ message }) => (
  <div>
    There was an error :(
    <div>{message}</div>
  </div>
)

export default Error;
