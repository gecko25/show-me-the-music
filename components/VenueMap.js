import React from 'react';
import PropTypes from 'prop-types';
import GoogleMap from 'google-map-react'
import GoogleMapMarker from './SVG/GoogleMapMarker'
import styles from '../styles/MapStyles'

const QuestionMark = () => (
    <div className="question-mark-marker">?</div>
)

const VenueMap = ({
  lat,
  lng,
  location,
}) => {
  let zoom = 15, isKnownLocation = true, KEY = { key: 'AIzaSyCY591DoZl4S6hHC7xyWUc3V8rbuy7xE9w' };
  let center = { lat, lng };

  if (!center.lat && !center.lng){
      center = {
          lat: location.lat,
          lng: lng
      }
      zoom = 11;
      isKnownLocation = false;
  }

  return (
      <div style={{ width: '400px', height: '400px' }}>
          <GoogleMap
              bootstrapURLKeys={KEY}
              resetBoundsOnResize={true}
              center={center}
              zoom={zoom}
              options={styles}
              >
              {isKnownLocation ? <GoogleMapMarker lat={center.lat} lng={center.lng}/> : <QuestionMark lat={center.lat} lng={center.lng}/>}
          </GoogleMap>
        </div>
  );
}

/**********************
  PROP TYPE VALIDATIORS
  **********************/
VenueMap.propTypes = {
  lat: PropTypes.number,
  lng: PropTypes.number,
}

export default VenueMap;
