import PropTypes from 'prop-types';

const Loader = () => (
  <div>
    Loading...
  </div>
)

export default Loader;
