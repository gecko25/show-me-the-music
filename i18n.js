import NextI18Next from 'next-i18next'

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'en',
  otherLanguages: ['es'],
  localeSubpaths: 'foreign',
  use: [],
})

export default NextI18NextInstance

/* Optionally, export class methods as named exports */
export const {
  appWithTranslation,
  withTranslation,
  i18n,
} = NextI18NextInstance

// import i18n from 'i18next';
// import { initReactI18next } from 'react-i18next';
//
// import en from './static/locales/en.js';
// import es from './static/locales/es.js';
//
// import LanguageDetector from 'i18next-browser-languagedetector';
// // not like to use this?
// // have a look at the Quick start guide
// // for passing in lng and translations on init
//
// i18n
//   // detect user language
//   // learn more: https://github.com/i18next/i18next-browser-languageDetector
//   .use(LanguageDetector)
//   // pass the i18n instance to react-i18next.
//   .use(initReactI18next)
//   // init i18next
//   // for all options read: https://www.i18next.com/overview/configuration-options
//   .init({
//     resources: {
//       en: {
//         translation: en,
//       },
//       es: {
//         translation: es,
//       },
//     },
//     fallbackLng: 'en',
//     debug: true,
//     interpolation: {
//       escapeValue: false, // not needed for react as it escapes by default
//     }
//   });
//
//
// export default i18n;
