import actionTypes from './action-types';
import initialState from './initialState';

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_EVENTS:
      return {
        events: action.events,
      }
    case actionTypes.SET_BASE_URL:
      return {
        events: action.baseUrl,
      }
    default:
      return state
  }
}

export default reducer;
