import axios from 'axios';
import { absoluteUrl, logger } from '../utils';
import { setEvents } from '../store/actions';

/*
 * @return array of both the events and the client location so we can initialize the landing page
 */
export const getEventsFromIp = async ({ baseUrl, clientip }) => {
  const url = `${baseUrl}api/events`;
  logger.info(`Going to get events: ${baseUrl}api/events with ip address: ${clientip}`);

  try {
    const { data } = await axios.get(`${baseUrl}api/events`, {
      headers: { 'x-client-ip': clientip },
    });
    if (data.resultsPage.status !== 'ok') throw new Error('Songkick API problem.');

    logger.info('Received event data', data.resultsPage.events.length);
    return data.resultsPage.events;

  } catch (error) {
    logger.error('Error getting event data');

    throw new Error(data.resultsPage.error ? data.resultsPage.error.message : error.message);
  }

  return null;
}

/*
 *
 */
export const getLocations = async ({ query, baseUrl }) => {
  try {
    logger.info(`Going to get locations: ${baseUrl}api/locations?location=${query}`);
    const { data } = await axios.get(`${baseUrl}api/locations?location=${query}`);
    const locations = data.map(loc => {
      const isCountry = loc.metroArea.country.displayName === loc.metroArea.displayName;
      if (isCountry) {
        return {
          id: loc.metroArea.id,
          displayName: loc.metroArea.displayName,
        }
      } else {
        const city = loc.metroArea.displayName;
        const state = loc.metroArea.state && loc.metroArea.state.displayName || '';
        const country = loc.metroArea.country.displayName || '';

        let displayName = '';
        if (state) {
          displayName = `${city}, ${state}`;
        } else {
          displayName = `${city}, ${country}`;
        }

        return {
          id: loc.metroArea.id,
          displayName,
        }
      }
    });

    logger.info(locations);
    return locations.splice(0,4);
  } catch (error) {
    logger.error('Error getting locations', error);
  }

}

/*
 *
 */
export const getEventsFromLocation = async ({ location, date, baseUrl }) => {
  const url = `${baseUrl}api/events`;
  const dateFormatted = date.format('YYYY-MM-DD');
  logger.info(`Going to get events: ${baseUrl}api/events?location=${location}&date=${dateFormatted}`);

  const { data } = await axios.get(`${baseUrl}api/events?location=${location}&date=${dateFormatted}`, {
    location,
    min_date: dateFormatted,
    max_date: dateFormatted,
  });

  try {
    if (data.resultsPage.status !== 'ok') throw new Error('Songkick API problem.');
    logger.info('Received event data', data.resultsPage.events[0]);
    return data.resultsPage.events;
  } catch (error) {
    throw new Error(data.resultsPage.error ? data.resultsPage.error.message : error.message);
  }
}

const api = {
  getEventsFromIp,
  getLocations,
  getEventsFromLocation
};

export default api;
