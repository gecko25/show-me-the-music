import actionTypes from './action-types';

export const setEvents = events => dispatch => {
  return dispatch({ type: actionTypes.SET_EVENTS, events })
}

export const setBaseUrl = baseUrl => dispatch => {
  return dispatch({ type: actionTypes.SET_BASE_URL, baseUrl })
}

export default setEvents;
