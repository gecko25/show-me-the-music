const axios = require('axios');
const { logger } = require('../config');

const lastfm = axios.create({
  baseURL: 'https://ws.audioscrobbler.com/2.0/',
  headers: {
    'Content-Type': 'application/json',
  },
  params: {
    api_key: process.env.LAST_FM_KEY,
  }
});


exports.getSimilarArtistsAndGenres = async function getSimilarArtists({ displayName, mbid }) {
	    logger.info('Going to get similar artists and genres from lastFM', displayName);

      try {
        const response = await lastfm.get('/', {
          params: {
            method: 'artist.getinfo',
            artist: displayName,
            mbid: mbid,
            format: 'json',
            api_key: process.env.LAST_FM_KEY, // ? Not being attached ??
          }
        });

        logger.info(response.request.path)

        const { similar, tags } = response.data.artist;

        const similarArtists = similar.artist.map(artist => artist.name);
        const genres = tags.tag.map(tag => tag.name);

        logger.info('Successfully received artists and genres from lastFM');
        logger.info('similarArtists', similarArtists);

        return {
          genres,
          similarArtists
        };
      } catch(error) {
        logger.warn('Unable to get genres/similar artists from lastfm. Returning empty arrays', error.data ? error.data : error);
        return {
          genres: [],
          similarArtists: [],
        }
      }

}
