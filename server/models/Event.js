var Performer = require('./Performer');
var Start = require('./Start');
var Venue = require('./Venue');

module.exports = {
    performers: [Performer],
    start: Start,
    venue: Venue,
    genres: [""],
    id: ''
}
