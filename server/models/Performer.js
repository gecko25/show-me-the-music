const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const performerSchema = new Schema({
   displayName: String,
   spotify: {
       artistId: String,
       displayName: String,
       top_tracks: Array,
       followers: Number,
       popularity: Number,
       followers: Number,
       play_url: String
   },
   songkick: {
       displayName: String,
       artistId: Number,
       uri: String,
   },
   genres: Array,
   isHeadliner: Boolean,
   popularity: Number,
   isAvailableOnSpotify: Boolean,
   similarArtists: Array,
   _id: Number,
   mbid: String,
	 summary: [{ language: String, summary: String}],
	 content: [{ language: String, content: String}],
   dateModified: String,
});

const Performer = mongoose.model('Performer', performerSchema);

module.exports = Performer;
