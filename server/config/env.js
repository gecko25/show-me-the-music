/**
 * Template taken from : https://github.com/pblair12/mosaic-groups/blob/master/server/config/config.js
 */
const env = process.env.NODE_ENV || 'development';
const localport = 3000;

const envs = {
  development: {
    db: {
        url: `mongodb+srv://sara:${process.env.MONGO_PASSWORD_LOCAL}@cluster0-5xtmz.mongodb.net/test?retryWrites=true&w=majority`,
    },
  },
  // staging: {
  //   db: {
  //       url: `mongodb+srv://sara:${process.env.MONGO_PASSWORD}@cluster0-pnuxc.mongodb.net/test?retryWrites=true&w=majority`,
  //   },
  // },
  production: {
    db: {
        url: `mongodb+srv://sara:${process.env.MONGO_PASSWORD_PROD}@cluster0-9wq1y.mongodb.net/test?retryWrites=true&w=majority`,
    },
  },
}

module.exports = envs[env];
