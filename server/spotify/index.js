const axios = require('axios');
const base64 = require('base-64');
const request = require('request-promise'); // In order submit old fashion form data we needed this library
const { buildPerformer } = require('./helpers');
const { logger } = require('../config');
const connectToDatabase = require('../config/mongoose')
const Performer = require ('../models/Performer');

const getClientAccessTokens = async function getClientAccessTokens(req) {
  const encodedString = base64.encode(`${process.env.SPOTIFY_CLIENT_ID}:${process.env.SPOTIFY_CLIENT_SECRET}`);

  try {
    const response = await request.post('https://accounts.spotify.com/api/token', {
        form: { grant_type: 'client_credentials' },
        headers: { 'Authorization': `Basic ${encodedString}` },
        json: true
    });

    req.session = { client_credentials: response };
    return Promise.resolve(response.access_token);

  } catch (error) {
    return Promise.reject(error);
  }
}


exports.findArtistOnSpotify = async function findArtistOnSpotify(req, res) {
  connectToDatabase();
  const { songkickArtistId, songkickArtistName } = req.query;
  logger.info(`Checking to see if songkick artist ${songkickArtistName} id:${songkickArtistId} exists in the database...`);

	Performer.findById(songkickArtistId, async (err, perf = {}) => {
		if (err) {
			logger.error('Error trying to find perfomer', err);
		}

		/* Performer info exists in the database, return immediately */
		if (perf) {
			logger.info('Performer found in show me database, returning 200', perf);
			res.status(200).json(perf);

			/* Performer does not exist in the database, we need to build it */
		} else {
			logger.info('Performer not found in database, going to build performer object');

      /* Check to see if we have access token from spotify */
      const cookieAvailable = req.session && req.session.client_credentials;
      if (!cookieAvailable) {
        const access_token = await getClientAccessTokens(req);
        logger.info(`Successfully received spotify access token ${access_token}. Going to search spotify.`);
      }

			const performer = await buildPerformer(req);

			// Save and return perfomer object
			performer.save((err, p) => {
				if (err) {
					return logger.error('Unable to save the db', err);
				}
				res.status(200).json(p);
				return logger.info('Successfully saved and returned performer object', p);
			});
		}

	});
};
