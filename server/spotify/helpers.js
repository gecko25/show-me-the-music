const axios = require('axios');
const { logger } = require('../config');
const Performer = require ('../models/Performer');
const { getSimilarArtistsAndGenres } = require ('../lastfm');

const spotify = access_token => (
  axios.create({
  baseURL: 'https://api.spotify.com/v1/',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + access_token
    }
  })
);

const getSpotifyArtistInfo = async function getSpotifyArtistInfo(req) {
	const { songkickArtistName } = req.query;
	/* Get spotify artist info  */
	logger.info(`Attempting to find songkick artist on on spotify`);
  try {
    const response = await spotify(req.session.client_credentials.access_token).get('/search', {
      params: {
        q: `"${songkickArtistName}"`, // quotations for a more exact match
        type: 'artist',
        limit: 1,
      }
    });

    const info = {
      spotifyArtistId: '',
      spotifyAristName: '',
      spotifyGenres: [],
      popularity: 0,
      followers: 0,
      play_url: '',
    }

    if (response.data.artists.items.length > 0) {
      const {
        id: spotifyArtistId,
        genres: spotifyGenres,
        name: spotifyAristName,
        popularity,
      } = response.data.artists.items[0];

      const followers = response.data.artists.items[0].followers.total;
      const play_url = response.data.artists.items[0].external_urls.spotify;

      logger.info(`Successfully retrieved spotify artist ${spotifyAristName} - ${spotifyArtistId}`);
      return Promise.resolve({ spotifyArtistId, spotifyAristName, spotifyGenres, popularity, followers, play_url });
    }

    logger.warn('Unable to find any matching spotify artists');
    return Promise.resolve(info);

  } catch (error) {
    logger.warn('Error occured trying to to find spotify artist', error);
    return Promise.resolve(info);
  }
}

const getSpotifyTopTracksFor = async function getSpotifyTopTracksFor(artistId, access_token) {
  logger.info('Going to get top tracks for artist');
	logger.info(`First going to get ip address: https://geo.ipify.org/api/v1?apiKey=${process.env.GEO_IP_KEY}`);
  const location = await axios(`https://geo.ipify.org/api/v1?apiKey=${process.env.GEO_IP_KEY}`);
	logger.info('Got country info', location.data)
  const { country } = location.data.location;

  try {
		logger.info(`Getting top tracks:  /artists/${artistId}/top-tracks?country=${country}`)
    const response = await spotify(access_token).get(`artists/${artistId}/top-tracks?country=${country}`);

    if (response.data.tracks.length > 0) {
      const topTracks = response.data.tracks.map(track => ({
        id: track.id,
        name: track.name,
      }));

      return topTracks;
    }

    logger.warn(`No top tracks found for: ${artidId}`)
    return [];

  } catch (error) {
    logger.warn('Error occured trying to spotify top tracks.', error);
    return [];
  }
}

exports.getGeoInfo = async function getGeoInfo(req) {
	// http://api.ipstack.com/check?access_key=558bfb82ef3f65209e317ad3a124d8a6&format=1&check
	// add this so we can get artist bio back in the right language
	// how to save this to db?
}

exports.buildPerformer = async function buildPerformer(req) {
  const { songkickArtistId, songkickArtistName, mbid } = req.query;

	try {
		const { spotifyArtistId, spotifyAristName, spotifyGenres, popularity, followers, play_url } = await getSpotifyArtistInfo(req);

    let topTracks = [];
    if (spotifyArtistId) {
      topTracks  = await getSpotifyTopTracksFor(spotifyArtistId, req.session.client_credentials.access_token);
    }

    // TODO: make this request concurrently while getting spotify top tracks.
    const { genres: lastFmGenres, similarArtists }  = await getSimilarArtistsAndGenres({ displayName: songkickArtistName, mbid });

		return new Performer({
			displayName: songkickArtistName,
			spotify: {
					artistId: spotifyArtistId,
					displayName: spotifyAristName,
					top_tracks: topTracks,
          popularity,
          followers,
          play_url,
			},
			songkick: {
					displayName: songkickArtistName,
					artistId: songkickArtistId,
					uri: String,
			},
      similarArtists: similarArtists,
			genres: [ ...new Set([...spotifyGenres, ...lastFmGenres]) ], // spotifyGenres.length > 0 ? spotifyGenres: lastFmGenres, //
			isAvailableOnSpotify: Boolean(spotifyArtistId),
			_id: songkickArtistId,
      mbid: mbid,
      lastUpdated: Date.now(),
		});

	} catch (error) {
		logger.error('Unable to build artist object');

		if (error.error) {
			logger.error(error);
		}

		logger.error(error.data);
    logger.error(error);
	}
}
