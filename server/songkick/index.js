const axios = require('axios');
const { buildEvents, getStartTimeData } = require('./helpers');
const { logger } = require('../config');

const songkick = axios.create({
  baseURL: 'https://api.songkick.com/api/3.0',
  headers: {
    'Content-Type': 'application/json',
  },
  params: {
		apikey: process.env.SONGKICK_KEY,
  }
});

const defaultError = {
    resultsPage: {
        status: "error",
        error: {
            message: "Unknown error",
        }
    }
}

const normalizeError = (error = {}) => {
  let status = 500, data = defaultError;

  if (error.message) {
    data.resultsPage.error.message = error.message;
  }

  if (error.response) {
    status = error.response.status;
    data = error.response.data;
  }

  logger.error(data.resultsPage.error.message);
  return { status, data }
}

const getEventsByIp = async function getEventsByIp(req, res) {
  try {
    // make sure we use clients ip and not the ip from the data center
    const ip = process.env.NODE_ENV === 'production' && req.headers['x-client-ip'];

    logger.info(`Going to get songkick events with ip address: `, process.env.NODE_ENV === 'production' ? `ip:${ip}` : 'clientip');

    const response = await songkick.get('/events.json', {
      params: {
        apikey: process.env.SONGKICK_KEY, // I dont know why this doesnt get configured as a default above :(
        location: process.env.NODE_ENV === 'production' ? `ip:${ip}` : 'clientip',
      }
    });

    if (response.data && response.data.resultsPage.status === 'ok') {
      // build event object the way we like it
      const events = buildEvents(response.data);

      // copy over the meta data
      const metaData = { ...response.data.resultsPage };
      delete metaData.results;

      // send back response with "show me" events (aka only data we need)
      res.status(200).json({ "resultsPage": { events, ...metaData }})
    } else {
      throw new Error('Songkick data return not ok')
    }

  } catch (error) {
    const { status, data } = normalizeError(error);
    res.status(status).json(data);
  }
}

exports.getEventInfo = async function getEventInfo(req, res) {
  try {
		logger.info('Going to get event info for event:', req.query.eventId);
    const { eventId } = req.query;
    const response = await songkick.get(`/events/${eventId}.json`, {
			params: {
				apikey: process.env.SONGKICK_KEY,
			}
		});

    if (response.data && response.data.resultsPage.status === 'ok') {
			logger.info('Successfully received event info');
      const eventInfo = {
        ...response.data,
        start: getStartTimeData(response.data.resultsPage.results.event),
      };

      res.status(200).json(eventInfo);
    } else {
      throw new Error('Unable to get event details from songkick data');
    }

  } catch (error) {
    const { status, data } = normalizeError(error);
    res.status(status).json(data);
  }
}

exports.getMetroAreaId = async function getMetroAreaId(req, res) {
  const { location } = req.query;
  logger.info('Going to find songkick locations with query', location);

  try {
    const response = await songkick.get('/search/locations.json', {
      params: {
        apikey: process.env.SONGKICK_KEY, // I dont know why this doesnt get configured as a default above :(
        query: location
      }
    })
    logger.info('Successfully received location ids', response.data);
    if (response.data && response.data.resultsPage.status === 'ok') {
      res.status(200).json(response.data.resultsPage.results.location);
    } else {
      throw new Error('Songkick data return not ok');
    }
  } catch (error) {
    const { status, data } = normalizeError(error);
    res.status(status).json(data);
  }
}

const getEventsByMetroId = async function getEventsByMetroId(req, res) {
  const { location, min_date, max_date } = req.query;
  logger.info('Going to find songkick events with location id:', location);

  try {
    const response = await songkick.get(`/metro_areas/${location}/calendar.json`, {
      params: {
        apikey: process.env.SONGKICK_KEY, // I dont know why this doesnt get configured as a default above :(
        metro_area_id: location,
        min_date,
        max_date,
      }
    })
    logger.info('Successfully received events', response.data);

    if (response.data && response.data.resultsPage.status === 'ok') {
      // build event object the way we like it
      const events = buildEvents(response.data);

      // copy over the meta data
      const metaData = { ...response.data.resultsPage };
      delete metaData.results;

      // send back response with "show me" events (aka only data we need)
      res.status(200).json({ "resultsPage": { events, ...metaData }})
    } else {
      throw new Error('Songkick data return not ok');
    }
  } catch (error) {
    const { status, data } = normalizeError(error);
    res.status(status).json(data);
  }
}


exports.getEvents = function getEvents(req, res) {
  logger.info('Going to get events based on query type', req.query);
  if (req.query.location) {
    getEventsByMetroId(req, res);
  } else {
    getEventsByIp(req, res);
  }
}


// get images from music brainz -- https://wiki.musicbrainz.org/Cover_Art_Archive/API
// get genre data from music brainz --
