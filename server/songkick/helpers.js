const moment = require('moment');

const getPerformerData =  function getPerformerData(songkickEvent){
    const _performers = [];

    songkickEvent.performance.forEach(function(songkick_performance){
        const _performer = {};
        _performer.displayName = songkick_performance.artist.displayName;

        _performer.songkick = {
            artist_id: songkick_performance.artist.id,
            displayName: songkick_performance.artist.displayName,
            uri: songkick_performance.artist.uri
        };

        _performer.isHeadliner = songkick_performance.billing === 'headline' ? true : false;
        _performers.push(_performer);
    });

    return _performers;
};

export const getStartTimeData = function getStartTimeData(songkickEvent){
    const _start = {};

    if (songkickEvent.start.date){
        _start.datetime = moment(songkickEvent.start.date);
        _start.displayDate = _start.datetime.format("MMM DD"); //Aug 12
        _start.displayDay = _start.datetime.format("dddd"); //Monday (use ddd for Mon)

    }

    if (songkickEvent.start.datetime) {
        const _date = new Date(songkickEvent.start.datetime);
        const hours = songkickEvent.start.time.substring(0, 2);
        const minutes = songkickEvent.start.time.substring(3, 5);
        _date.setHours(hours);
        _date.setMinutes(minutes);
        if (minutes === '00'){
            _start.displayTime = moment(_date).format("h a").toUpperCase(); //7PM
        }
        else{
            _start.displayTime = moment(_date).format("h:mm a").toUpperCase(); //7:30PM
        }
    }else{
        _start.displayTime = 'TBD'

    }

    return _start;
};

const getVenueData = function getVenueData(songkickEvent){
    const _venue = {};
    _venue.songkickId = songkickEvent.venue.id;
    _venue.displayName = songkickEvent.venue.displayName;
    _venue.lat = songkickEvent.venue.lat;
    _venue.lng = songkickEvent.venue.lng;

    return _venue;
};

const createEventModelFrom = function createEventModelFrom(songkickEvent){
    const _event = Object.create(songkickEvent);
    _event.performers = getPerformerData(songkickEvent);
    _event.start = getStartTimeData(songkickEvent);
    _event.venue = getVenueData(songkickEvent);
    _event.location = songkickEvent.location;
    _event.id = songkickEvent.id;

    return _event;
};

export const buildEvents = function buildEvents(response) {
  let events = [];

  if (response.resultsPage.totalEntries > 0) {
      try {
        events = response.resultsPage.results.event.map(songkick_event => createEventModelFrom(songkick_event))
        .filter( (songkick_event) => {
            if (songkick_event.performers.length > 0){
                return songkick_event;
            }
        });
      } catch (error) {
        throw new Error(error.message);
      }
  }

  return events;
}
